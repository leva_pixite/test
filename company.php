<?php
namespace Pixite\Crm;

use Bitrix\Crm\Binding\ContactCompanyTable;
use Bitrix\Crm\Requisite\LinkTable;
use \Bitrix\Main\Config\Option;
use Bitrix\Main\Data\Cache;
use \Pixite\Common;
use \Bitrix\Main\Error;
use Bitrix\Crm\CompanyTable;
use Pixite\Exchange\RabbitMQImport;
use Bitrix\Crm\UtmTable;
use Bitrix\Crm\Service\Container;
use Bitrix\Crm\Integrity\DuplicateCommunicationCriterion;
use Bitrix\Crm\EntityAddress;
use Bitrix\Crm\EntityAddressType;
use Bitrix\Crm\Counter\EntityCounterManager;
use Bitrix\Crm\Counter\EntityCounterType;

\CModule::IncludeModule('crm');

class Company extends \Pixite\CRMObject
{
    const TABLE_NAME_COMPANY_UF = 'b_uts_crm_company';
    const BULK_COMPANY_NAME = 'Компания не указана';

    /*
    * Загрузка компании
    */
    /**
     * @var mixed|string
     */
    private string $LAST_ERROR = '';

    public function import($arFields, $delete)
    {
        if (array_key_exists('RQ_INN', $arFields)) {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'call importReq', true);
            $this->importReq($arFields, $arFields['PARENT'], $delete);
        } else {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'call importCompany', true);
            $this->importCompany($arFields, $delete);
        }

        return $this->result;

    }

    /*
    * Загрузка компании
    */
    protected function importCompany($arFields, $delete)
    {
        //бизнес-регион
        $regionCode = Common::getOption('COMPANY_REGION');

        if (!empty($arFields[$regionCode]) && $arFields[$regionCode] != '00000000-0000-0000-0000-000000000000') {
            $region = $this->getRegion(['XML_ID' => $arFields[$regionCode]], ['ID']);

            if(!empty($region['ID']))
                $arFields[$regionCode] = $region['ID'];
        } else {
            unset($arFields[$regionCode]);
        }

        //Основная отрасль
        $branchCode = Common::getOption('COMPANY_BRANCH');

        if (!empty($arFields[$branchCode]) && $arFields[$branchCode] != '00000000-0000-0000-0000-000000000000') {
            $branch = $this->getBranch(['XML_ID' => $arFields[$branchCode]], ['ID']);

            if (!empty($branch['ID']))
                $arFields[$branchCode] = $branch['ID'];
        } else {
            unset($arFields[$branchCode]);
        }

        //Ответственный
        $woAssignedUser = false;

        if ($arFields['ASSIGNED_BY_ID'] == '00000000-0000-0000-0000-000000000000') {
            $woAssignedUser = true;
            unset($arFields['ASSIGNED_BY_ID']);
        }

        if (!empty($arFields['ASSIGNED_BY_ID']) && $arFields['ASSIGNED_BY_ID'] != '00000000-0000-0000-0000-000000000000') {
            $User = new \Pixite\Main\User;
            $assigned = $User->searchUserByGUID($arFields['ASSIGNED_BY_ID']);

            if (!empty($assigned['ID'])) {
                $arFields['ASSIGNED_BY_ID'] = $assigned['ID'];
            } else {
                unset($arFields['ASSIGNED_BY_ID']);
            }
        } else {
            unset($arFields['ASSIGNED_BY_ID']);
        }

        //Моя компания - не нужен ответственный
        if ($arFields['IS_MY_COMPANY'] == 'Y') {
            $woAssignedUser = true;
            unset($arFields['ASSIGNED_BY_ID']);
        }

        if (!$woAssignedUser) {
            if (empty($arFields['ASSIGNED_BY_ID'])) {
                $this->result->addError(new Error('Не найден ответственный'));
                return $this->result;
            }
        }

        $arFields['ORIGIN_ID'] = $arFields['XML_ID'];
        unset($arFields['XML_ID']);

        //Моя компания
        if ($arFields['IS_MY_COMPANY'] == 'Y') {
            $arRequisite = $arFields['REQ'];
            unset($arFields['REQ']);
        }

        if ($arFields[Common::UF_CLIENT_TYPE]) {
            $elemID = 0;

            $res = \CIBlockElement::getList([], ['NAME' => $arFields[Common::UF_CLIENT_TYPE], 'IBLOCK_ID' => 41], false, [], ['ID']);

            if ($row = $res->fetch()) {
                $elemID = $row['ID'];
            }

            $arFields[Common::UF_CLIENT_TYPE] = $elemID;
        }


        $company = $this->searchCompanyByGUID($arFields[Common::getOption('COMPANY_GUID')]);

        $arFields['OPENED'] = 'Y';

        if ($delete) {
            $arFields['OPENED'] = 'N';
        }

        if (empty($company)) {
            if(!empty($arFields[Common::getOption('COMPANY_EXPORT_IN_1С')]))
                $arFields[Common::getOption('COMPANY_1C_URL')] = \Pixite\Crm\Helper::generateUrlFor1C($arFields[Common::getOption('COMPANY_GUID')], 'company');

            if ($bulkContactId = Contact::getBulkContactId()) {
                $arFields['CONTACT_ID'] = [
                    $bulkContactId
                ];
            }

            $this->add($arFields);

            RabbitMQImport::logTime(__METHOD__, __LINE__, 'company added', true);
        } else {
            $arCheckChangedFields = [
                'ADDRESS',
                'ADDRESS_2',
                'ADDRESS_CITY',
                'ADDRESS_POSTAL_CODE',
                'ADDRESS_REGION',
                'ADDRESS_PROVINCE',
                'ADDRESS_COUNTRY',
                'ADDRESS_LOC_ADDR_ID',
            ];

            if (isset($arFields['ADDRESS'])) {
                $currCompany = new \Bitrix\Crm\CompanyTable();

                $arCompany = $currCompany::getById($company['ID']);

                if (!is_array($arCompany)) {
                    $arCompany = $arCompany->Fetch();
                }

                if ($arCompany) {
                    if ($arFields['ADDRESS'] == $arCompany['ADDRESS']) {
                        unset($arFields['ADDRESS']);
                    }
                }
            }

            if (!isset($arFields['CONTACT_ID']) || empty($arFields['CONTACT_ID'])) {
                $arContactsId = ContactCompanyTable::getCompanyContactIDs($company['ID']);

                if (empty($arContactsId)) {
                    if ($bulkContactId = Contact::getBulkContactId()) {
                        $arFields['CONTACT_ID'] = [
                            $bulkContactId
                        ];
                    }
                }
            }

        //    $this->update($company['ID'], $arFields);

        //    RabbitMQImport::logTime(__METHOD__, __LINE__, 'company updated', true);
        }

        if (!empty($arRequisite)) {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'importReq after company add ', true);
            $this->importReq($arRequisite, $arFields[Common::getOption('COMPANY_GUID')], false);
        }

        $arFields['XML_ID'] = $arFields[Common::getOption('COMPANY_GUID')];

        $Element = new \Pixite\Main\ElementMappingSave;

        $Element->save($this->result, $arFields, 'company', false);

        return $this->result;
    }

    /*
    * Загрузка реквизитов компании
    */
    protected function importReq($arFields, $companyGuid, $delete)
    {
        unset($arFields['PARENT']);

        $company = $this->searchCompanyByGUID($companyGuid);
        $arFields['ACTIVE'] = 'Y';

        if ($delete) {
            $arFields['ACTIVE'] = 'N';
        }

        if (!empty($company)) {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'found company '.$company['ID']);

            $monitor = array(
                'time_start' => microtime(true),
                'memory_start' => memory_get_usage()
            );

            $arRequisites = $this->requisiteGetList($company['ID'], $arFields['PRESET_ID'], ['ID', 'NAME', 'RQ_INN', 'ENTITY_ID', 'XML_ID']);

            RabbitMQImport::logTime(__METHOD__, __LINE__, 'search requisite');

            $is_add = false;

            if (!empty($arRequisites)) {
                foreach ($arRequisites as $req) {
                    if($req['XML_ID'] == $arFields['XML_ID']) {
                        $requisite = new \Bitrix\Crm\EntityRequisite();
                        //pixite kord fix: delete by ID not ENTITY_ID $requisite->deleteByEntity(\CCrmOwnerType::Company, $req['ENTITY_ID']);
                        $arFields['MODIFY_BY_RABBIT'] = 1;
                        $res = $requisite->update($req['ID'], $arFields);
//                        RabbitMQImport::logTime(__METHOD__, __LINE__, 'updated old requisite '.$req['ID'].' / '.$req['NAME'] . ' / ' . $res, true);
                        if (is_array($arFields['ADDRESS'])){
                            CompanyAddress::addOrUpdate($req['ID'], \Bitrix\Crm\EntityAddress::Primary, $arFields['ADDRESS']);
                        }

                        if (is_array($arFields['ADDRESS_EXT'])){
                            foreach ($arFields['ADDRESS_EXT'] as $key => $fieldAddr){
                                CompanyAddress::addOrUpdate($req['ID'], $fieldAddr['TYPE_ADDR'], $fieldAddr);
                            }
                        }
                        $is_add = true;
                        RabbitMQImport::logTime(__METHOD__, __LINE__, 'delete old requisite '.$req['ID'].' / '.$req['RQ_INN'], true);
                    }
                }
            }

            if (!$is_add || empty($arRequisites)) {
                $arFields['ENTITY_ID'] = $company['ID'];
                $req_new = $this->addReq($arFields);
                RabbitMQImport::logTime(__METHOD__, __LINE__, 'add requisite');

                if ($req_new) {
                    if (is_array($arFields['ADDRESS'])) {
                        CompanyAddress::addOrUpdate($req_new, \Bitrix\Crm\EntityAddress::Primary, $arFields['ADDRESS']);
                    }

                    if (is_array($arFields['ADDRESS_EXT'])) {
                        foreach ($arFields['ADDRESS_EXT'] as $key => $arField) {
                            CompanyAddress::addOrUpdate($req_new, $arField['TYPE_ADDR'], $arField);
                        }
                    }
                } else {
                    $this->result->addError(new Error('Не удалось добавить реквизиты для компании ' . $company['ID']));
                }
            }
        } else {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'company not found '.$companyGuid);
            $this->result->addError(new Error('Не найдена компания'));
        }

        return $this->result;
    }

    /*
    * Поиск компании по GUID
    */
    public function searchCompanyByGUID($xmlId)
    {
        if (!$xmlId) {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'company not found - empty XML_ID', true);
            return false;
        }

        try {
            $ufCompanyXmlId = Common::UF_COMPANY_XML_ID;
            $connection = \Bitrix\Main\Application::getConnection();
            $query = 'SELECT VALUE_ID FROM '.self::TABLE_NAME_COMPANY_UF.' WHERE '.$ufCompanyXmlId.' = "'.$xmlId.'" LIMIT 1';
            $queryResult = $connection->query($query);

            if ($ar = $queryResult->fetch()) {
                if ($ar['VALUE_ID']) {
                    $company = [
                        'ID' => $ar['VALUE_ID']
                    ];
                }
            }

            if (!empty($company)) {
                RabbitMQImport::logTime(__METHOD__, __LINE__, 'company found - '.self::TABLE_NAME_COMPANY_UF, true);
                return $company;
            }

            return false;
        }
        catch (\Exception $e) {

        }

        $dbRes = \CCrmCompany::GetListEx(
            [
                'ID' => 'ASC'
            ],
            [
                Common::UF_COMPANY_XML_ID => $xmlId,
                'CHECK_PERMISSIONS' => 'N'
            ],
            [
                'ID'
            ]
        );

        $result = $dbRes->Fetch();

        if ($result) {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'company found - CCrmCompany::GetListEx', true);
        } else {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'company not found', true);
        }

        return $result;
    }

    /*
    * Добавление компании
    */
    public function add($arFields)
    {
        $company = new \CCrmCompany(false);
        $companyId = $company->Add($arFields);

        if ($companyId) {
            $this->result->setData(['result' => $companyId]);
        } else {
            $this->result->addError(new Error(__METHOD__.' '.__LINE__.' / '.$company->LAST_ERROR));
        }
    }

    /*
    * Добавление реквизитов компании
    */
    public function addReq($arFields)
    {
        $arFields['ENTITY_TYPE_ID'] = \CCrmOwnerType::Company;

        $address = $arFields['ADDRESS'];
        unset($arFields['ADDRESS']);

        $fm['FM'] = $arFields['COMPANY'];
        unset($arFields['COMPANY']);

        $requisite = new \Bitrix\Crm\EntityRequisite();

        $this->result = $requisite->add($arFields);

        if ($this->result->isSuccess()) {
            if (!empty($address)) {
                $this->addReqAddress($this->result->getId(), $address);
            }

            return $this->result->getId();
        } else {
            $this->result->addError(new Error(__METHOD__.' '.__LINE__.' / ' . implode(',', $this->result->getErrorMessages())));
        }

        return false;
    }

    /*
    * Обновление реквизитов компании
    */
    public function updateReq($id, $arFields)
    {
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $this->result = $requisite->update($id, $arFields);
    }

    /*
    * Обновление компании
    */
    public function update($companyId, $arFields)
    {
        $company = new \CCrmCompany(false);
        $Helper = new \Pixite\Crm\Helper;

        $arFieldsMulti = $Helper->getCrmFieldMulti($companyId, 'COMPANY');

        if (!empty($arFieldsMulti)) {
            foreach($arFields['FM'] as $key => $fields) {
                foreach($fields as $keyField => $field) {
                    foreach($arFieldsMulti as $fieldMulti) {
                        if ($fieldMulti['TYPE_ID'] == $key && $fieldMulti['VALUE_TYPE'] == 'WORK') {
                            if(trim($fieldMulti['VALUE']) == trim($field['VALUE']))
                                unset($arFields['FM'][$key][$keyField]);
                        }

                        if(!isset($arFields['FM'][$fieldMulti['TYPE_ID']])) {
                            $fieldMultiObj = new \CCrmFieldMulti();
                            $fieldMultiObj->Delete($fieldMulti['ID']);
                        }
                    }
                }
            }
        }

        RabbitMQImport::logTime(__METHOD__, __LINE__, 'company before update', true);

        $options = [
            'CURRENT_USER' => Common::DEFAULT_EXCHANGE_USER_ID,
            'REGISTER_SONET_EVENT' => false,
            'DISABLE_USER_FIELD_CHECK' => true,
            'ENABLE_DUP_INDEX_INVALIDATION' => false,
        ];

        if ($company->Update($companyId, $arFields, false, false, $options)) {
            RabbitMQImport::logTime(__METHOD__, __LINE__, 'company after update', true);
            $this->result->setData(['result' => $companyId]);
        } else {
            $this->result->addError(new Error(__METHOD__.' '.__LINE__.' / '.$companyId.' / '.$company->LAST_ERROR));
        }
    }

    public function fastUpdate($ID, array &$arFields, array $arOptions = []) : bool
    {
        $company_class = new \CCrmCompany();

        global $DB;

        $ID = (int)$ID;

        if (!is_array($arOptions)) {
            $arOptions = [];
        }

        $arRow = \Bitrix\Crm\CompanyTable::getById($ID)->fetch();

        if (empty($arRow)) {
            return false;
        }

        $iUserId = Common::DEFAULT_EXCHANGE_USER_ID;

        unset(
            $arFields['DATE_CREATE'],
            $arFields['DATE_MODIFY'],
            $arFields['CATEGORY_ID']
        );

        if (empty(trim($arFields['TITLE']))) {
            unset($arFields['TITLE']);
        }

        $arFields['~DATE_MODIFY'] = $DB->CurrentTimeFunction();

        if (!isset($arFields['MODIFY_BY_ID']) || $arFields['MODIFY_BY_ID'] <= 0) {
            $arFields['MODIFY_BY_ID'] = $iUserId;
        }

        if (isset($arFields['ASSIGNED_BY_ID']) && $arFields['ASSIGNED_BY_ID'] <= 0) {
            unset($arFields['ASSIGNED_BY_ID']);
        }

        if (isset($arFields['REVENUE'])) {
            $arFields['REVENUE'] = floatval($arFields['REVENUE']);
        }

        if (isset($arFields['IS_MY_COMPANY'])) {
            $arFields['IS_MY_COMPANY'] = $arFields['IS_MY_COMPANY'] === 'Y' ? 'Y' : 'N';
        }

        $assignedByID = (int)($arFields['ASSIGNED_BY_ID'] ?? $arRow['ASSIGNED_BY_ID']);
        $categoryId = (int)($arRow['CATEGORY_ID'] ?? 0);

        $bResult = false;

        $arOptions['CURRENT_FIELDS'] = $arRow;

        $Helper = new \Pixite\Crm\Helper;
        $CCrmFieldMulti = new \CCrmFieldMulti();

        $arFieldsMulti = $Helper->getCrmFieldMulti($ID, 'COMPANY');

        if (!empty($arFieldsMulti)) {
            foreach($arFields['FM'] as $key => $fields) {
                foreach($fields as $keyField => $field) {
                    foreach($arFieldsMulti as $fieldMulti) {
                        if ($fieldMulti['TYPE_ID'] == $key && $fieldMulti['VALUE_TYPE'] == 'WORK') {
                            if (trim($fieldMulti['VALUE']) == trim($field['VALUE'])) {
                                unset($arFields['FM'][$key][$keyField]);
                            }
                        }

                        if (!isset($arFields['FM'][$fieldMulti['TYPE_ID']])) {
                            $CCrmFieldMulti->Delete($fieldMulti['ID']);
                        }
                    }
                }
            }
        }

        if (!$company_class->CheckFields($arFields, $ID, $arOptions)) {
            $this->LAST_ERROR = &$company_class->LAST_ERROR;
        } else {
            if (!isset($arFields['ID'])) {
                $arFields['ID'] = $ID;
            }

            $arFields['OPENED'] = !empty($arFields['OPENED']) ? $arFields['OPENED'] : $arRow['OPENED'];

            if (isset($arFields['LOGO'])) {
                unset($arFields['LOGO']);
            }

            if (isset($arFields['HAS_EMAIL'])) {
                unset($arFields['HAS_EMAIL']);
            }

            if (isset($arFields['HAS_PHONE'])) {
                unset($arFields['HAS_PHONE']);
            }

            if (isset($arFields['HAS_IMOL'])) {
                unset($arFields['HAS_IMOL']);
            }

            unset($arFields['ID']);

            $sUpdate = $DB->PrepareUpdate('b_crm_company', $arFields, 'FILE: '.__FILE__.'<br /> LINE: '.__LINE__);

            if ($sUpdate <> '') {
                $DB->Query("UPDATE b_crm_company SET {$sUpdate} WHERE ID = {$ID}", false, 'FILE: '.__FILE__.'<br /> LINE: '.__LINE__);
                $bResult = true;
            }

            if (defined('BX_COMP_MANAGED_CACHE')) {
                $arNameFields = ['TITLE'];

                $bClear = false;

                foreach ($arNameFields as $val) {
                    if (isset($arFields[$val])) {
                        $bClear = true;

                        break;
                    }
                }

                if ($bClear) {
                    $GLOBALS['CACHE_MANAGER']->ClearByTag('crm_entity_name_' . \CCrmOwnerType::Company . '_' . $ID);
                }
            }

            \CCrmEntityHelper::NormalizeUserFields($arFields, $company_class::$sUFEntityID, $GLOBALS['USER_FIELD_MANAGER'], ['IS_NEW' => false]);

            $GLOBALS['USER_FIELD_MANAGER']->Update($company_class::$sUFEntityID, $ID, $arFields);

            if (isset($arFields['ADDRESS'])
                || isset($arFields['ADDRESS_2'])
                || isset($arFields['ADDRESS_CITY'])
                || isset($arFields['ADDRESS_POSTAL_CODE'])
                || isset($arFields['ADDRESS_REGION'])
                || isset($arFields['ADDRESS_PROVINCE'])
                || isset($arFields['ADDRESS_COUNTRY'])
                || isset($arFields['ADDRESS_LOC_ADDR_ID']))
            {
                EntityAddress::register(
                    \CCrmOwnerType::Company,
                    $ID,
                    EntityAddressType::Primary,
                    [
                        'ADDRESS_1' => isset($arFields['ADDRESS'])
                            ? $arFields['ADDRESS'] : (isset($arRow['ADDRESS']) ? $arRow['ADDRESS'] : null),
                        'ADDRESS_2' => isset($arFields['ADDRESS_2'])
                            ? $arFields['ADDRESS_2'] : (isset($arRow['ADDRESS_2']) ? $arRow['ADDRESS_2'] : null),
                        'CITY' => isset($arFields['ADDRESS_CITY'])
                            ? $arFields['ADDRESS_CITY'] : (isset($arRow['ADDRESS_CITY']) ? $arRow['ADDRESS_CITY'] : null),
                        'POSTAL_CODE' => isset($arFields['ADDRESS_POSTAL_CODE'])
                            ? $arFields['ADDRESS_POSTAL_CODE'] : (isset($arRow['ADDRESS_POSTAL_CODE']) ? $arRow['ADDRESS_POSTAL_CODE'] : null),
                        'REGION' => isset($arFields['ADDRESS_REGION'])
                            ? $arFields['ADDRESS_REGION'] : (isset($arRow['ADDRESS_REGION']) ? $arRow['ADDRESS_REGION'] : null),
                        'PROVINCE' => isset($arFields['ADDRESS_PROVINCE'])
                            ? $arFields['ADDRESS_PROVINCE'] : (isset($arRow['ADDRESS_PROVINCE']) ? $arRow['ADDRESS_PROVINCE'] : null),
                        'COUNTRY' => isset($arFields['ADDRESS_COUNTRY'])
                            ? $arFields['ADDRESS_COUNTRY'] : (isset($arRow['ADDRESS_COUNTRY']) ? $arRow['ADDRESS_COUNTRY'] : null),
                        'COUNTRY_CODE' => isset($arFields['ADDRESS_COUNTRY_CODE'])
                            ? $arFields['ADDRESS_COUNTRY_CODE'] : (isset($arRow['ADDRESS_COUNTRY_CODE']) ? $arRow['ADDRESS_COUNTRY_CODE'] : null),
                        'LOC_ADDR_ID' => isset($arFields['ADDRESS_LOC_ADDR_ID'])
                            ? (int)$arFields['ADDRESS_LOC_ADDR_ID'] : (isset($arRow['ADDRESS_LOC_ADDR_ID']) ? (int)$arRow['ADDRESS_LOC_ADDR_ID'] : 0),
                        'LOC_ADDR' => isset($arFields['ADDRESS_LOC_ADDR']) ? $arFields['ADDRESS_LOC_ADDR'] : null
                    ]
                );
            }

            if (isset($arFields['REG_ADDRESS'])
                || isset($arFields['REG_ADDRESS_2'])
                || isset($arFields['REG_ADDRESS_CITY'])
                || isset($arFields['REG_ADDRESS_POSTAL_CODE'])
                || isset($arFields['REG_ADDRESS_REGION'])
                || isset($arFields['REG_ADDRESS_PROVINCE'])
                || isset($arFields['REG_ADDRESS_COUNTRY'])
                || isset($arFields['REG_ADDRESS_LOC_ADDR_ID']))
            {
                EntityAddress::register(
                    \CCrmOwnerType::Company,
                    $ID,
                    EntityAddressType::Registered,
                    [
                        'ADDRESS_1' => isset($arFields['REG_ADDRESS'])
                            ? $arFields['REG_ADDRESS'] : (isset($arRow['REG_ADDRESS']) ? $arRow['REG_ADDRESS'] : null),
                        'ADDRESS_2' => isset($arFields['REG_ADDRESS_2'])
                            ? $arFields['REG_ADDRESS_2'] : (isset($arRow['REG_ADDRESS_2']) ? $arRow['REG_ADDRESS_2'] : null),
                        'CITY' => isset($arFields['REG_ADDRESS_CITY'])
                            ? $arFields['REG_ADDRESS_CITY'] : (isset($arRow['REG_ADDRESS_CITY']) ? $arRow['REG_ADDRESS_CITY'] : null),
                        'POSTAL_CODE' => isset($arFields['REG_ADDRESS_POSTAL_CODE'])
                            ? $arFields['REG_ADDRESS_POSTAL_CODE'] : (isset($arRow['REG_ADDRESS_POSTAL_CODE']) ? $arRow['REG_ADDRESS_POSTAL_CODE'] : null),
                        'REGION' => isset($arFields['REG_ADDRESS_REGION'])
                            ? $arFields['REG_ADDRESS_REGION'] : (isset($arRow['REG_ADDRESS_REGION']) ? $arRow['REG_ADDRESS_REGION'] : null),
                        'PROVINCE' => isset($arFields['REG_ADDRESS_PROVINCE'])
                            ? $arFields['REG_ADDRESS_PROVINCE'] : (isset($arRow['REG_ADDRESS_PROVINCE']) ? $arRow['REG_ADDRESS_PROVINCE'] : null),
                        'COUNTRY' => isset($arFields['REG_ADDRESS_COUNTRY'])
                            ? $arFields['REG_ADDRESS_COUNTRY'] : (isset($arRow['REG_ADDRESS_COUNTRY']) ? $arRow['REG_ADDRESS_COUNTRY'] : null),
                        'COUNTRY_CODE' => isset($arFields['REG_ADDRESS_COUNTRY_CODE'])
                            ? $arFields['REG_ADDRESS_COUNTRY_CODE'] : (isset($arRow['REG_ADDRESS_COUNTRY_CODE']) ? $arRow['REG_ADDRESS_COUNTRY_CODE'] : null),
                        'LOC_ADDR_ID' => isset($arFields['REG_ADDRESS_LOC_ADDR_ID'])
                            ? (int)$arFields['REG_ADDRESS_LOC_ADDR_ID'] : (isset($arRow['REG_ADDRESS_LOC_ADDR_ID']) ? (int)$arRow['REG_ADDRESS_LOC_ADDR_ID'] : 0),
                        'LOC_ADDR' => isset($arFields['REG_ADDRESS_LOC_ADDR']) ? $arFields['REG_ADDRESS_LOC_ADDR'] : null
                    ]
                );
            }

            \CCrmEntityHelper::NormalizeUserFields($arFields, $company_class::$sUFEntityID, $GLOBALS['USER_FIELD_MANAGER'], ['IS_NEW' => false]);

            $GLOBALS['USER_FIELD_MANAGER']->Update($company_class::$sUFEntityID, $ID, $arFields);

            \Bitrix\Crm\Statistics\CompanyGrowthStatisticEntry::synchronize($ID, ['ASSIGNED_BY_ID' => $assignedByID]);
            \Bitrix\Crm\Activity\CommunicationStatistics::synchronizeByOwner(\CCrmOwnerType::Company, $ID, ['ASSIGNED_BY_ID' => $assignedByID]);

            if ($bResult) {
                $previousAssignedByID = isset($arRow['ASSIGNED_BY_ID']) ? (int)$arRow['ASSIGNED_BY_ID'] : 0;

                if (($assignedByID > 0 || $previousAssignedByID > 0) || $assignedByID !== $previousAssignedByID) {
                    $assignedByIDs = [];

                    if ($assignedByID > 0) {
                        $assignedByIDs[] = $assignedByID;
                    }

                    if ($previousAssignedByID > 0) {
                        $assignedByIDs[] = $previousAssignedByID;
                    }

                    EntityCounterManager::reset(
                        EntityCounterManager::prepareCodes(
                            \CCrmOwnerType::Company,
                            [
                                EntityCounterType::PENDING,
                                EntityCounterType::OVERDUE,
                                EntityCounterType::ALL,
                            ],
                            [
                                'CATEGORY_ID' => $categoryId,
                                'EXTENDED_MODE' => true,
                            ]
                        ),
                        $assignedByIDs
                    );
                }
            }

            if (isset($arFields['FM']) && is_array($arFields['FM'])) {
                $CCrmFieldMulti->SetFields(\CCrmOwnerType::CompanyName, $ID, $arFields['FM']);

                $multifields = DuplicateCommunicationCriterion::prepareEntityMultifieldValues(\CCrmOwnerType::Company, $ID);

                $hasEmail = $CCrmFieldMulti::HasValues($multifields, $CCrmFieldMulti::EMAIL) ? 'Y' : 'N';

                $hasPhone = $CCrmFieldMulti::HasValues($multifields, $CCrmFieldMulti::PHONE) ? 'Y' : 'N';

                $hasImol = $CCrmFieldMulti::HasImolValues($multifields) ? 'Y' : 'N';

                if (
                    $hasEmail !== (isset($arRow['HAS_EMAIL']) ? $arRow['HAS_EMAIL'] : 'N')
                    ||
                    $hasPhone !== (isset($arRow['HAS_PHONE']) ? $arRow['HAS_PHONE'] : 'N')
                    ||
                    $hasImol !== (isset($arRow['HAS_IMOL']) ? $arRow['HAS_IMOL'] : 'N')
                ) {
                    $DB->Query("UPDATE b_crm_company SET HAS_EMAIL = '{$hasEmail}', HAS_PHONE = '{$hasPhone}', HAS_IMOL = '{$hasImol}' WHERE ID = {$ID}", false, 'FILE: '.__FILE__.'<br /> LINE: '.__LINE__);

                    $arFields['HAS_EMAIL'] = $hasEmail;
                    $arFields['HAS_PHONE'] = $hasPhone;
                    $arFields['HAS_IMOL'] = $hasImol;
                }
            }

            $duplicateCriterionRegistrar = \Bitrix\Crm\Integrity\DuplicateManager::getCriterionRegistrar(\CCrmOwnerType::Company);

            $data =
                (new \Bitrix\Crm\Integrity\CriterionRegistrar\Data())
                    ->setEntityTypeId(\CCrmOwnerType::Company)
                    ->setEntityId($ID)
                    ->setCurrentFields($arFields)
                    ->setPreviousFields($arRow);

            $duplicateCriterionRegistrar->update($data);

            UtmTable::updateEntityUtmFromFields(\CCrmOwnerType::Company, $ID, $arFields);

            Container::getInstance()->getParentFieldManager()->saveParentRelationsForIdentifier(
                new \Bitrix\Crm\ItemIdentifier(\CCrmOwnerType::Company, $ID),
                $arFields
            );

            $arFields['ID'] = $ID;

            $originalContactBindings = ContactCompanyTable::getCompanyBindings($ID);

            $contactBindings = null;

            if (isset($arFields['CONTACT_ID']) && is_array($arFields['CONTACT_ID'])) {
                $arFields['CONTACT_ID'] = array_filter($arFields['CONTACT_ID']);

                $contactBindings = \Bitrix\Crm\Binding\EntityBinding::prepareEntityBindings(\CCrmOwnerType::Contact, $arFields['CONTACT_ID']);

                if (empty($arFields['CONTACT_ID'])) {
                    ContactCompanyTable::unbindAllContacts($arFields['ID']);
                } else {
                    $arCurrentContact = \Bitrix\Crm\Binding\EntityBinding::prepareEntityIDs(\CCrmOwnerType::Contact, $originalContactBindings);
                    $arAdd = array_diff($arFields['CONTACT_ID'], $arCurrentContact);
                    $arDelete = array_diff($arCurrentContact, $arFields['CONTACT_ID']);

                    ContactCompanyTable::bindContactIDs($arFields['ID'], $arAdd);
                    ContactCompanyTable::unbindContactIDs($arFields['ID'], $arDelete);

                    if (isset($GLOBALS['USER'])) {
                        if (!class_exists('CUserOptions')) {
                            include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/classes/' . $GLOBALS['DBType'] . '/favorites.php');
                        }

                        \CUserOptions::SetOption('crm', 'crm_contact_search', ['last_selected' => implode(',', $arAdd)]);
                    }
                }
            }

            \CCrmEntityHelper::registerAdditionalTimelineEvents([
                'entityTypeId' => \CCrmOwnerType::Company,
                'entityId' => $ID,
                'fieldsInfo' => $company_class::GetFieldsInfo(),
                'previousFields' => $arRow,
                'currentFields' => $arFields,
                'options' => $arOptions,
                'bindings' => [
                    'entityTypeId' => \CCrmOwnerType::Contact,
                    'previous' => $originalContactBindings,
                    'current' => $contactBindings
                ]
            ]);

            \Bitrix\Crm\Search\SearchContentBuilderFactory::create(\CCrmOwnerType::Company)->build($ID, ['checkExist' => true]);

            \Bitrix\Crm\Timeline\CompanyController::getInstance()->onModify(
                $ID,
                [
                    'CURRENT_FIELDS' => $arFields,
                    'PREVIOUS_FIELDS' => $arRow,
                    'OPTIONS' => $arOptions
                ]
            );

            if ($bResult && isset($arFields['ASSIGNED_BY_ID'])) {
                \CCrmSonetSubscription::ReplaceSubscriptionByEntity(
                    \CCrmOwnerType::Company,
                    $ID,
                    \CCrmSonetSubscriptionType::Responsibility,
                    $arFields['ASSIGNED_BY_ID'],
                    $arRow['ASSIGNED_BY_ID'],
                    false
                );
            }
        }

        return $bResult;
    }

    /*
    * Получение реквизитов компании
    */
    public function requisiteGetList($companyId, $presetId, $arSelect = [])
    {
        $requisite = new \Bitrix\Crm\EntityRequisite();
        $dbRes = $requisite->getList(['filter' => ['ENTITY_ID' => $companyId, 'PRESET_ID' => $presetId, 'ENTITY_TYPE_ID' => \CCrmOwnerType::Company], 'select' => $arSelect]);

        $arRequisite = [];

        while ($req = $dbRes->fetch()) {
            $arRequisite[] = $req;
        }

        return $arRequisite;
    }

    /*
    * Добавление адреса компании
    */
    public function addReqAddress($reqId, $arFields, $type_id = 6)
    {
        $address = new \Bitrix\Crm\EntityAddress();

        $address->register(
            8,
            $reqId,
            $type_id,
            $arFields
        );
    }

    public function fastUpdateField($companyId, $arFields)
    {
        $beforeEvents = GetModuleEvents('crm', 'OnBeforeCrmCompanyUpdate');

        while ($arEvent = $beforeEvents->Fetch()) {
            if (ExecuteModuleEventEx($arEvent, [&$arFields]) === false) {
                $this->LAST_ERROR = 'Не удалось запустить событие обновления компании';

                return false;
            }
        }

        $res = CompanyTable::update($companyId, $arFields);

        $afterEvents = GetModuleEvents('crm', 'OnAfterCrmCompanyUpdate');

        while ($arEvent = $afterEvents->Fetch()) {
            ExecuteModuleEventEx($arEvent, [&$arFields]);
        }

        return $res->isSuccess();
    }

    public function updateField($companyId, $arFields)
    {
        $this->update($companyId, $arFields);

        return $this->result;
    }

    /*
    * Поиск компании
    */
    public function search($arFilter, $arSelect = ['*'], $limit = false)
    {
        $arParams = [
            'order' => [
                'ID' => 'ASC'
            ],
            'select' => $arSelect,
            'filter' => $arFilter
        ];

        if ($limit !== false) {
            $limit = intval($limit);

            if ($limit > 0) {
                $arParams['limit'] = $limit;
            }
        }

        $result = CompanyTable::getList($arParams)->fetchAll();

        if (!is_array($result)) {
            return false;
        }

        return $result;
    }

    /*
    * Поиск компании по ID
    */
    public function searchById($companyId, $arSelect = ['*', 'UF_*'])
    {
        $arParams = [
            'filter' => [
                'ID' => $companyId
            ],
            'select' => $arSelect,
        ];

        $result = CompanyTable::getList($arParams)->fetch();

        if (!is_array($result)) {
            return false;
        }

        return $result;
    }

    /*
    * Поиск компании
    */
    protected function delete($id)
    {
        $company = new \Bitrix\Crm\Entity\Company;
        $company->delete($id);
    }

    /*
    * Получение главных реквизитов компании
    */
    static public function getMainRequisite($companyId, $xmlId = false)
    {
        $result = [];
        $requisite = new \Bitrix\Crm\EntityRequisite();

        $dbRes = $requisite->getList(
            [
                'filter' => [
                    'ENTITY_ID' => $companyId,
                    'ENTITY_TYPE_ID' => \CCrmOwnerType::Company
                ],
                'select' => [
                    '*',
                    Common::UF_REQUISITE_COMPANY_REG_NUMBER
                ]
            ]
        );

        while ($req = $dbRes->fetch()) {
            $result[$req['ID']] = $req;
        }

        $arSettings = $requisite->loadSettings(\CCrmOwnerType::Company, $companyId);

        if ($mainId = $arSettings['REQUISITE_ID_SELECTED']) {
            if (isset($result[$mainId])) {
                return $result[$mainId];
            }
        }

        return reset($result);
    }

    /**
     * @param $companyId
     * @param $requisiteId
     * @throws \Bitrix\Main\ArgumentException
     */
    static public function setMainRequisite($companyId, $requisiteId)
    {
        if ($companyId && $requisiteId) {
            $requisiteLink = new \Bitrix\Crm\Requisite\LinkTable();

            $dbRequisiteLink = $requisiteLink->getList(
                [
                    'filter' => [
                        'ENTITY_TYPE_ID' => \CCrmOwnerType::Company,
                        'ENTITY_ID' => $companyId
                    ],
                    'select' => [
                        'ENTITY_ID'
                    ]
                ]
            );

            if ($arRequisiteLink = $dbRequisiteLink->fetch()) {
                $primary = [
                    'ENTITY_TYPE_ID' => \CCrmOwnerType::Company,
                    'ENTITY_ID' => $arRequisiteLink['ENTITY_ID']
                ];

                LinkTable::update($primary, [
                    'REQUISITE_ID' => $requisiteId
                ]);
            }
        }
    }

    /*
    * Получение списка моих компаний
    */
    static public function getOrganizations()
    {
        $Company = new Company;
        $cache_id = 'TEKO.CRM.MYCOMPANY';
        $arItems = [];
        $obCache = new \CPHPCache();

        if ($obCache->InitCache(3600, $cache_id . 'crm', '/crm/company')){
            $arItems = $obCache->GetVars();
        }

        elseif ($obCache->StartDataCache())	{
            $arCompanies = $Company->search(['IS_MY_COMPANY' => 'Y'], ['ID', Common::getOption('COMPANY_GUID')]);

            foreach($arCompanies as $company) {
                if ($xmlId = $company[Common::getOption('COMPANY_GUID')]) {
                    $arItems[$xmlId] = $company['ID'];
                }
            }

            $obCache->EndDataCache($arItems);
        }

        return $arItems;
    }

    /*
    * Получение списка моих компаний
    */
    static public function getMyOrganizationXmlId($companyId)
    {
        $result = '';
        $arAll = self::getOrganizations();

        foreach($arAll as $xmlId => $id) {
            if ($companyId == $id || $companyId == 'CO_'.$id) {
                $result = $xmlId;
                break;
            }
        }

        return $result;
    }

    /*
    * Получение данных моих компаний
    */
    public function getOrganizationData($arFilter = [], $arSelect = ['*'])
    {
        $arFilter['IS_MY_COMPANY'] = 'Y';
        $arCompanies = Company::search($arFilter, $arSelect);
        $arItems = [];

        foreach($arCompanies as $company) {
            $arItems[$company['ID']] = $company;
        }

        return $arItems;
    }

    /*
    * Получение типов компании
    */
    public function mappingTypeBy1C($typeBX = false, $type1C = false)
    {
        $arType = [
            '4' => 'Перевозчик',
            '3' => 'ПрочиеОтношения',
            'COMPETITOR' => 'Конкурент',
            'SUPPLIER' => 'Поставщик',
            'CUSTOMER' => 'Клиент',
        ];

        if ($typeBX && !empty($arType[$typeBX])) {
            return $arType[$typeBX];
        }

        if ($type1C && $key = array_search($type1C, $arType)) {
            return $key;
        }

        return false;
    }

    /*
    * Получение данных региона
    */
    public function getRegion($arFilter, $arSelect)
    {
        $List = new \Pixite\Directory\Lists;
        $List->setIblockId(Option::get(SITE_MODULE, 'PX_IBLOCK_REGION'));
        $region = current($List->search($arFilter, $arSelect));

        if (!empty($region)) {
            return $region;
        }

        return false;
    }

    /*
    * Получение данных отрасли
    */
    public function getBranch($arFilter, $arSelect)
    {
        $List = new \Pixite\Directory\Lists;
        $List->setIblockId(Option::get(SITE_MODULE, 'PX_IBLOCK_BRANCH'));

        $branch = current($List->search($arFilter, $arSelect));

        if (!empty($branch)) {
            return $branch;
        }

        return false;
    }

    /*
    * Проверка выгрузки компании в 1С
    */
    static public function isExportedTo1C($id)
    {
        $Company = new Company;

        $company = current($Company->search(['ID' => $id], ['ID', Common::getOption('COMPANY_EXPORT_IN_1С')]));

        return !empty($company[Common::getOption('COMPANY_EXPORT_IN_1С')]);
    }

    public function deleteAll($arCompanies) {
        $Company = new \Bitrix\Crm\Entity\Company;

        foreach ($arCompanies as $company) {
            $Company->delete($company['ID']);
        }
    }


    /*
    *  Поиск компании по реквизитам
    */
    public function searchByRequisite($arFilter) {

        if (!empty($arFilter['INN'])) {
            $requisite = new \Bitrix\Crm\EntityRequisite();
            $dbRes = $requisite->getList(['filter' => ['RQ_INN' => $arFilter['INN'], 'ENTITY_TYPE_ID' => \CCrmOwnerType::Company], 'select' => ['ENTITY_ID']]);

            while ($req = $dbRes->fetch()) {
                return $req['ENTITY_ID'];
            }
        }

        if (!empty($arFilter['EMAIL'])) {
            $res = \CCrmFieldMulti::GetList(
                ['ID' => 'asc'],
                [
                    'TYPE_ID' => 'EMAIL',
                    'ENTITY_ID' => 'COMPANY',
                    'VALUE' => $arFilter['EMAIL']
                ]
            );

            while($ar = $res->Fetch()) {
                return $ar['ELEMENT_ID'];
            }
        }

        return false;
    }

    /**
     * @return int|mixed
     * @throws \Bitrix\Main\ArgumentException
     */
    static public function getBulkCompanyId()
    {
        $companyId = 0;
        $cache = Cache::createInstance();

        if ($cache->initCache(3600, __CLASS__ . __FUNCTION__, 'app')) {
            $companyId = $cache->getVars();
        } elseif ($cache->startDataCache()) {
            $db = CompanyTable::getList([
                'order' => ['ID' => 'DESC'],
                'filter' => [
                    '=TITLE' => self::BULK_COMPANY_NAME,
                ],
                'limit' => 1
            ]);

            if ($ar = $db->fetch()) {
                $companyId = $ar['ID'];
            }

            $cache->endDataCache($companyId);
        }

        return $companyId;
    }
}